﻿namespace Klingsten.ApiClient4.Model {
    public class UserRequest {
        public string S { get; set; }
        public string Key { get; set; }
        public string q { get; set; }
        public int ServiceId { get; set; }

        public override string ToString() {
            return $"Service={S} | key={Key} | Query={q}";
        }
    }
}
