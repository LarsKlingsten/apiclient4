﻿using System;
using Klingsten.DirectiveClassLibrary.Models;

namespace Klingsten.ApiClient4.Model {

    public static class SD {

        public const MicroService microService = MicroService.All; // set the role of this server (all or for a specific service)

        public const string API_PASSWORD = "pass";

        public const string BASE_URI = "https://authenticationserver5.azurewebsites.net/";
        public const string REQUEST_AUTH = "api/Request/Auth/";
        public const string USE_DIRECTIVE = "api/Directive/";
        public const string USE_REQUEST = "api/Directive/UseRequest/";

        /*
        Useful links:

        This service makes calls: 

        http://authenticationserver5.azurewebsites.net/api/directive/ListServices/{pwd}  // list available services +  ID 
        http://authenticationserver5.azurewebsites.net/api/Directive/UseDirective/{pwd}/{ServiceName}/{ClientID}    
        http://authenticationserver5.azurewebsites.net/api/Directive/UseRequest/{pwd}/{ServiceName}  

        http://authenticationserver5.azurewebsites.net/api/Request/Auth/{API_PASSWORD}/{serviceId}/{apiKey}"); 
      
          
        http://apiclient4.azurewebsites.net/api/values/logstr/{pwd} // (Logs as strings)
        http://apiclient4.azurewebsites.net/api/values/log/{pwd}    // (Logs as JObject)
             
       
        */


    }
}
