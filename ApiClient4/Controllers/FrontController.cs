﻿using System.Web.Http;
using Klingsten.ApiClient4.Service;
using Klingsten.ApiClient4.Model;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;

namespace Klingsten.ApiClient4.Controllers {

    public class FrontController : ApiController {

        private AuthService authService = new AuthService();
        private DirectiveService directiveService = DirectiveService.GetInstance();

        [HttpPut]
        [HttpPost]
        [HttpGet]
        public ApiResponse Get([FromUri]UserRequest request) {

            ServiceLog log = ServiceLog.GetInstance();


            VmDirective vmDirective = directiveService.GetDirective();
            if (vmDirective == null) {
                return new ApiResponse(HttpCode.Internal_Server_Error, "Panic. Could not Load Directive. Service Fails. Check Auth Server logs (IP is not authorized)", null);
            }
            
            VmService vmService = directiveService.GetService(request);
            if (vmService == null) {
                return MyStatic.SERVICES_DOES_NOT_EXIST;
            }
            
            System.Net.Http.HttpMethod httpMethod = Request.Method;
            RequestResponse requestResponse = authService.Authorize(request, vmService, httpMethod, vmDirective);

            if (!requestResponse.IsAuthorized) {
                return new ApiResponse(HttpCode.Unauthorized, "Unauthorized", requestResponse.ToJObjectIgnoreNull());
            }

            //
            // make the actual request to a sub microservice // 
            // intensionally not implemented as not in scope of this project
            //

            // sample reply to show what the Authentication said - real life would return the data from an
            // actual microservice
            return new ApiResponse(HttpCode.OK, "Authorized", requestResponse.ToJObjectIgnoreNull());
        }
    }
}