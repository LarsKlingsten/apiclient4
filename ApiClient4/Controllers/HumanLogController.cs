﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;

namespace ApiClient4.Controllers {
    public class HumanLogController : Controller {

        private IDao daoService = DaoService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();

        public ActionResult Index() {
            List<LogEntry> logs = log.GetLogs().OrderByDescending(s => s.Updated).ToList(); ;
            return View(logs);
        }
    }
}
