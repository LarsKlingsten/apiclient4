﻿using System.Web.Http;
using Klingsten.ApiClient4.Service;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;

namespace Klingsten.ApiClient4.Controllers {

    public class LogController : ApiController {

        private AuthService authService = new AuthService();
        private DirectiveService directiveService = DirectiveService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();

        [HttpGet]
        [Route("api/Log/json/{pwd}")]
        public ApiResponse Log(string pwd) {
            if (!pwd.Equals(MyStatic.PASSWORD_FOR_SERVICE)) {
                return MyStatic.API_SERVICE_PASSWORD_REJECTED;
            }
            return new ApiResponse(HttpCode.OK, "LogService", new { log = log.GetLogs() }.ToJObjectIgnoreNull());
        }

        [HttpGet]
        [Route("api/Log/human/{pwd}")]
        public ApiResponse LogStr(string pwd) {

            if (!pwd.Equals(MyStatic.PASSWORD_FOR_SERVICE)) {
                return MyStatic.API_SERVICE_PASSWORD_REJECTED;
            }
            return new ApiResponse(HttpCode.OK, "VmDirective", new { log = log.GetLogsAsString() }.ToJObjectIgnoreNull());
        }

        [HttpGet]
        [Route("api/log/Directive/{pwd}")]
        public ApiResponse CurrentDirective(string pwd) {

            if (!pwd.Equals(MyStatic.PASSWORD_FOR_SERVICE)) {
                return MyStatic.API_SERVICE_PASSWORD_REJECTED;
            }

            var directive = directiveService.GetDirective();

            return new ApiResponse(HttpCode.OK, "VmDirective", directive.ToJObject());
        }
    }
}