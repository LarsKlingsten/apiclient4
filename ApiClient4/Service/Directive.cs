﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Klingsten.ApiClient4.Model;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using System.Linq;

namespace Klingsten.ApiClient4.Service {

    public class DirectiveService {

        private VmDirective _vmDirective;
        private ServiceLog log;
        private DateTime NEXT_UPDATE;
        private Stopwatch sw;
        private UserStatService userStatService;
        private static DirectiveService instance;

        public static DirectiveService GetInstance() {
            if (instance == null) {
                instance = new DirectiveService();
            }
            return instance;
        }

        private DirectiveService() {
            NEXT_UPDATE = MyStatic.Now();
            sw = new Stopwatch();
            log = ServiceLog.GetInstance();
            userStatService = UserStatService.GetInstance();
        }

        public static readonly DateTime SERVER_START = DateTime.UtcNow;

        /// summary:
        ///          The clientID must as unque ID - is required when more than one intance of the service is
        ///          used. The recommended method to store the record the datetime in a static variable and take the 
        ///          ticks (as is done here below). 
        ///          Warning: if incorrectly implemented this cause the Authentication server
        ///          may cause the Auth service to belive multiple instances are running, and this will eventuallly 
        ///          cause users access limits to be limited to one or zero requests. Alternatively may give user
        ///          limits multiplied with number of instances.

        public string ClientID() {
            return SD.microService + "-" + SERVER_START.Ticks;
        }

        public VmService GetService(UserRequest userRequest) {
            VmService vmService = GetDirective().Services.FirstOrDefault(s => s.name.Equals(userRequest.S));
            if (vmService == null) {
                log.AddLog(Status.Warning, $"ApiKey='{ userRequest.Key  }' requested '{userRequest.S}'. Does not exist");
            }
            return vmService;
        }

        private Uri GetDirectiveUri() {
            return new Uri(SD.BASE_URI + SD.USE_DIRECTIVE + SD.API_PASSWORD + "/" + SD.microService + "/" + ClientID());
        }

        private readonly static object myLock = new object();
        private bool isBusyFetchingDirective = false;

        public VmDirective GetDirective() {
            sw.Restart();
            if (_vmDirective == null) {
                lock (myLock) {
                    log.AddLog(Status.Created, $"Directives loads", sw);
                    FetchAndSetDirective();

                    // failed to fetch
                    if (_vmDirective == null) {
                        return null;
                    }

                    NEXT_UPDATE = MyStatic.Now().AddSeconds(_vmDirective.ReloadSeconds);
                }
            }
            else if (NEXT_UPDATE < MyStatic.Now()) {
                Task.Run(() => FetchAndSetDirective());
                NEXT_UPDATE = MyStatic.Now().AddSeconds(_vmDirective.ReloadSeconds);
            }
            return _vmDirective;
        }

        public void FetchAndSetDirective() {
            sw.Restart();

            if (!isBusyFetchingDirective) {
                isBusyFetchingDirective = true;

                ApiResponse apiResponse = new AuthApiComm().CallAuthenticationServer(GetDirectiveUri());

                if (apiResponse.StatusCode == HttpCode.Internal_Server_Error) {
                    log.AddLog(Status.Panic, apiResponse.Message);
                }
                else if (apiResponse.StatusCode == HttpCode.Not_Modified) {
                    log.AddLog(Status.Unchanged, $"Directive UseDirective={_vmDirective.UseDirective} Reloads={_vmDirective.ReloadSeconds} msg={apiResponse.Message}", sw);

                }
                else if (apiResponse.JSon == null) {
                    log.AddLog(Status.Warning, apiResponse.Message, sw);
                }
                else {
                    _vmDirective = apiResponse.JSon.ToObject<VmDirective>();
                    userStatService.Reset();
                    log.AddLog(Status.Updated, $"Directive UseDirective={_vmDirective.UseDirective} Reloads={_vmDirective.ReloadSeconds}", sw);
                }

                isBusyFetchingDirective = false;
            }
        }
    }
}