﻿// (c) Lars Klingsten 2015-11-25
using System;
using System.Diagnostics;
using Klingsten.ApiClient4.Model;
using Klingsten.DirectiveClassLibrary.Models;
using Klingsten.DirectiveClassLibrary.Services;
using System.Net.Http;

namespace Klingsten.ApiClient4.Service {
    public class AuthService {

        private Stopwatch sw = new Stopwatch();
        private DirectiveService directiveService = DirectiveService.GetInstance();
        private ServiceLog log = ServiceLog.GetInstance();
        private UserStatService userStatService = UserStatService.GetInstance();

        /// summary:
        ///       Loads or reload directives ( via GetDirective() ) and determines whether
        ///       the user request should be authorized by directive or request to the 
        ///       authentication server. 
        public RequestResponse Authorize(UserRequest userRequest, VmService vmService,  HttpMethod httpMethod, VmDirective vmDirective) {

            if (directiveService.GetDirective().UseDirective) {
                return AuthorizeDirective(userRequest, vmService, httpMethod, vmDirective);
            }
            else {
                return AuthorizeRequest(userRequest, vmService);
            }
        }

        /// summary: Calls the Auth microservice and parses the ApiResponse, and (if succcesful)
        ///          further parses the RequestReponse.
        ///          {
        ///             "IsAuthorized": true,
        ///             "Reset": 4,
        ///             "Limit": 30,
        ///             "Remaining": 25
        ///          } 
        /// 
        ///          the URI is called as follows:
        ///          http://authenticationserver5.azurewebsites.net/api/Request/Auth/{pwd}?serviceId={serviceName}&intentifier={ApiKey}
        ///  
        ///          sample: 
        ///          http://authenticationserver5.azurewebsites.net/api/Request/Auth/pass?serviceId=StoreProduct&intentifier=lars

        public RequestResponse AuthorizeRequest(UserRequest userRequest, VmService vmService) {

            sw.Restart();

            RequestResponse requestResponse = new RequestResponse();
            Uri uri = GetUriToAuthRequest(userRequest.Key, vmService.name);

            ApiResponse apiResponse = new AuthApiComm().CallAuthenticationServer(uri);

            if (apiResponse.StatusCode == HttpCode.Internal_Server_Error) {
                log.AddLog(Status.Panic, apiResponse.ToString(), sw);
            }
            else if (apiResponse.StatusCode == HttpCode.Not_Modified) {
                log.AddLog(Status.Info, $"{apiResponse.Message}", sw);
            }
            else if (apiResponse.JSon == null) {
                requestResponse.Message = apiResponse.Message;
                log.AddLog(Status.Warning, apiResponse.ToString(), sw);
            }
            else {
                requestResponse = apiResponse.JSon.ToObject<RequestResponse>();
                log.AddLog(Status.Info, userRequest.Key + " " + requestResponse.ToString(), sw);
            }

            return requestResponse;
        }
        /// summary:  
        ///           Returns to URI for the Auth Service. See details for SD (static class) at SDStaticData 
        private Uri GetUriToAuthRequest(string apiKey, string serviceName) { // 
            return new Uri($"{SD.BASE_URI}{SD.REQUEST_AUTH}{SD.API_PASSWORD}/{serviceName}/{apiKey}");
        }

        /// summary:  
        ///           Serves no particular purpose other than establishing that asp.net mvc is using threads
        ///           (presently it is not used) 
        private void LogThreadInfo() {
            var t = System.Threading.Thread.CurrentThread;
            int workers;
            int completion;
            System.Threading.ThreadPool.GetAvailableThreads(out workers, out completion); ;
            log.AddLog(Status.Info, $"Threads id={t.ManagedThreadId}  isThreadPool={t.IsThreadPoolThread } workers={workers} completion={ completion}");
        }

        /// summary:  Authorized users request without calling the Auth Service. Instead it uses
        ///           the VmDirective (that reloads regularly on each user request) to authorize 
        ///           or reject requests. It utilizes userStatService (similarly to what is done 
        ///           at the Auth service).
        ///           The userStatService utilizes a Dictionary with to ensure to ensure that 
        ///           previous user request are handled swiftly.  
        public RequestResponse AuthorizeDirective(UserRequest userRequest, VmService vmService, HttpMethod httpMethod, VmDirective vmDirective) {
            sw.Restart();

            string key = userStatService.GetKey(userRequest.Key, vmService.name);
            UserStat userStat = userStatService.GetUserStatByKey(key);   // get userStat from previous request call

            if (userStat == null) { // is users first call
                VmUserService vmUserService = vmService.userServices.Find(s => s.ApiKey == userRequest.Key);  // is user is authorized for Service
                if (vmUserService == null) {
                    return new RequestResponse { IsAuthorized = false, Message = "User has not access to service" };
                }
                userStat = userStatService.CreateUserStat(userRequest.Key, userRequest.S, vmUserService, vmDirective);
            }
            
            if (! userStat.VmUserService.HasHttpMethodRights (httpMethod)) {
                return new RequestResponse { IsAuthorized = false, Message = $"User has has no 'http{httpMethod}' rights" };
            }
            
            userStat.addCount();

            // Exceeds limits
            bool withinLimit = userStatService.IsWithinLimits(userStat);
            log.AddLog(Status.Info, $"{userRequest.Key} {userStat} Authorized={withinLimit}", sw);

            if (!withinLimit) {
                userStat.addCountDeclined();
                return new RequestResponse {
                    IsAuthorized = false,
                    Message = "Too Many Requests. Number of requests exceeds limit.",
                    Reset = userStat.NextResetInSeconds(),
                    Limit = userStat.VmUserService.Limit
                };
            }

            // All is good          
            return new RequestResponse {
                IsAuthorized = true,
                Reset = userStat.NextResetInSeconds(),
                Limit = userStat.VmUserService.Limit,
                Remaining = userStat.Remaining
            };
        }
    }
}