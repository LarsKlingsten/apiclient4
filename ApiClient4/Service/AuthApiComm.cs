﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Klingsten.DirectiveClassLibrary.Models;

namespace Klingsten.ApiClient4.Service {

    public class AuthApiComm {

        public ApiResponse CallAuthenticationServer(Uri uri) {

            WebRequest webRequest = WebRequest.Create(uri);
            webRequest.Proxy = null;

            try {
                WebResponse myWebResponse = webRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                string htmlBody = streamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();
                ApiResponse apiResponse = ParseStringToApiResponse(htmlBody);
                return apiResponse;
            }
            catch (WebException e) {
                ApiResponse apiResonse = new ApiResponse(HttpCode.Internal_Server_Error, e.Message.ToString().Left(75), null);
                apiResonse.StatusCode = HttpCode.Internal_Server_Error;
                return apiResonse;
            }
        }

        public ApiResponse ParseStringToApiResponse(string jsonString) {
            return JsonConvert.DeserializeObject<ApiResponse>(jsonString);
        }
    }
}